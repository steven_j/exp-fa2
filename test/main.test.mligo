#import "tezos-ligo-fa2/lib/fa2/asset/single_asset.mligo" "SingleAsset"
#import "tezos-ligo-fa2/test/fa2/single_asset.test.mligo" "SingleAsset_helper"
#import "tezos-ligo-fa2/test/helpers/list.mligo" "List_helper"
#import "../src/main.mligo" "MyContract"

let originate_fa2 () = 
    (* fa2 with Test.originate *)

    let init_storage, owners, ops = SingleAsset_helper.get_initial_storage(10n, 10n, 10n) in
    let (taddr, _, _) = Test.originate SingleAsset.main init_storage 0tez in
    let contr = Test.to_contract taddr in
    let addr = Tezos.address contr in

    (* fa2 with Test.originate_from_file *)

    (* let f = "tezos-ligo-fa2/lib/fa2/asset/single_asset.mligo" in *)
    (* tezos-ligo-fa2/lib/fa2/asset/single_asset.mligo: No such file or directory  *)
    let f = "./test/single_asset.mligo" in
    let init_storage, owners, ops = SingleAsset_helper.get_initial_storage(10n, 10n, 10n) in
    let v_mich = Test.run (fun (x:SingleAsset.Storage.t) -> x) init_storage in
    let (addr, _, _) = Test.originate_from_file f "main" ([]: string list) v_mich 0tez in
    let taddr : (SingleAsset.parameter, SingleAsset.storage) typed_address = Test.cast_address addr in
    let contr = Test.to_contract taddr in
    {
        addr = addr;
        (* taddr = taddr; *)
        (*
File "./test/main.test.mligo", line 32, characters 8-11:
 31 | let test =
 32 |     let fa2 = originate_fa2() in
 33 |

Expected address but got typed_address (sum[Balance_of -> record[callback -> contract (list (record[balance -> nat , request -> record[owner -> address , token_id -> nat]])) , requests -> list (record[owner -> address , token_id -> nat])] , Transfer -> list (record[from_ -> address , tx -> list (record[amount -> nat , to_ -> address])]) , Update_operators -> list (sum[Add_operator -> record[operator -> address , owner -> address , token_id -> nat] , Remove_operator -> record[operator -> address , owner -> address , token_id -> nat]])] ,
record[ledger -> big_map (address , nat) , operators -> big_map (address , set (address)) , token_metadata -> record[token_id -> nat , token_info -> map (string , bytes)]])

        *)
        owners = owners;
        ops = ops;
        contr = contr;
    }

let test =
    let fa2 = originate_fa2() in

    (* contract under test *)
    let (taddr, _, _) = Test.originate MyContract.main fa2.addr 0tez in
    let contr = Test.to_contract taddr in
    let addr =  Tezos.address contr in
    let () = Test.log("My contract:") in
    let () = Test.log(addr) in

    (* add contract address as operator *)
    let sender_ = List_helper.nth_exn 0 fa2.owners in
    let () = Test.set_source sender_ in
    let () = Test.log(sender_) in
    let add_operator = [Add_operator({
        owner = sender_;
        operator = addr;
        token_id = 0n;
    })] in
    let res = Test.transfer_to_contract fa2.contr (Update_operators(add_operator)) 0mutez in
    let () = Test.log(res) in

    (* call contract *)
    let res = Test.transfer_to_contract contr () 0mutez in
    let () = Test.log(res) in

    let packed = Bytes.pack (fun() -> 
        match (Tezos.get_entrypoint_opt "%transfer" fa2.addr : SingleAsset.transfer contract option) with
        Some(c) ->
            let transfer_requests = ([
              ({from_=addr; tx=([{to_=sender_;amount=2n}] : SingleAsset.atomic_trans list)});
            ] : SingleAsset.transfer) in
            let op = Tezos.transaction transfer_requests 0mutez c in [op]
        | None -> failwith("CONTRACT_NOT_FOUND")
    ) in
    let () = Test.log(packed) in

    (* assertions *)
    let fa2_taddr : (SingleAsset.parameter, SingleAsset.storage) typed_address = Test.cast_address fa2.addr in
    let fa2_storage = Test.get_storage fa2_taddr in
    let () = match (Big_map.find_opt addr fa2_storage.ledger) with
        None -> failwith ("Should have found contract address")
        | Some(amt) -> assert (amt = 5n)
    in

    Test.log(fa2_storage)
