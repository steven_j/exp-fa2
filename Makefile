SHELL := /bin/bash

LIGO=@ligo
ifeq (, $(shell which ligo))
	LIGO=@docker run -v "$(PWD):$(PWD)" -w "$(PWD)" --rm -i ligolang/ligo:next 
endif

project_root=--project-root .

help:
	@grep -E '^[ a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | \
	awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'

compile = $(LIGO) compile contract $(project_root) -p hangzhou ./src/$(1) -o ./compiled/$(2) $(3)
test = $(LIGO) run test $(project_root) ./test/$(1) --protocol hangzhou

.PHONY: test
test: ## run tests
	$(call test,main.test.mligo)

compile: ## compile contracts
	@if [ ! -d ./compiled ]; then mkdir -p ./compiled ; fi
	$(call compile,main.mligo,mycontract.tz)

clean: ## clean up
	@rm -rf compiled

install: ## install
	$(LIGO) install
