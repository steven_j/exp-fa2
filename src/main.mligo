#import "tezos-ligo-fa2/lib/fa2/asset/single_asset.mligo" "SingleAsset"

type parameter = unit
type storage = address
type result = operation list * storage

let get_transfer_entrypoint (addr:address) : SingleAsset.transfer contract =
    match (Tezos.get_entrypoint_opt "%transfer" addr : SingleAsset.transfer contract option) with
          Some c -> c
        | None -> failwith "FA2 NOT FOUND" 

let transfer (s: storage) : operation =
    let dest = get_transfer_entrypoint (s) in
    let transfer_requests = ([
      ({from_=Tezos.source; tx=([{to_=Tezos.self_address;amount=5n}] : SingleAsset.atomic_trans list)});
    ] : SingleAsset.transfer) in
    Tezos.transaction transfer_requests 0mutez dest

let main (_action, store : parameter * storage) : result =
    let op = transfer(store) in ([op], store)
