# Exp FA2

1. Run `make install` to install dependencies
2. Just run `make to see available commands`

## Tips

You can use [entr](https://github.com/eradman/entr) have quick feedback loop, example:

```sh
fd -g "**/*.mligo" . | entr make test
```
